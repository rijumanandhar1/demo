import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor() { }

  getBooks(){
    return [
      {
        title:"one",
        description:"one description"
      },
      {
        title:"two",
        description:"two description"
      }
    ]
  }
}
