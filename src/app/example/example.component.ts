import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.css']
})
export class ExampleComponent implements OnInit {

  books;
  title:string = "List of books"

  constructor(bookService:BooksService) {
    this.books = bookService.getBooks();
    console.log(this.books)
   }

  ngOnInit(): void {
  }

}
